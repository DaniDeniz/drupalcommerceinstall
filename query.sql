DROP DATABASE IF EXISTS commerce_db;
CREATE DATABASE commerce_db;
CREATE USER 'commerce_dbuser'@'%' IDENTIFIED BY 'tasi18';

GRANT ALL PRIVILEGES ON commerce_db.* TO 'commerce_dbuser'@'%' WITH GRANT OPTION;

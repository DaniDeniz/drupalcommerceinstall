#!/bin/bash


yum update -y;

yum install -y httpd;

systemctl enable httpd

yum install -y epel-release

wget http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
rpm -Uvh remi-release-7*.rpm


yum install -y yum-utils

yum install -y php56 php56-php php56-php-mysqlnd php56-php-opcache php56-php-gd php56-php-mcrypt php56-php-mbstring php56-php-xml php56-php-cli

service httpd restart

yum install -y mariadb mariadb-server
service mariadb restart
/usr/bin/mysql_secure_installation

echo "Introduzca la contraseña de la Base de Datos: "
cat query.sql | mysql -p


systemctl enable mariadb

yum install -y phpmyadmin

cp -f httpd.conf /etc/httpd/conf/

cp -f my.cnf /etc/my.cnf


if [ $1 == "myelectro" ]
then

    cp backup_drupal_*.tar.gz /var/www/html
    
    cd /var/www/html
    
    tar -xzf backup_drupal_*.tar.gz 
  
    #cd /var/www/html/drupal/sites/default/

    service mariadb restart

    echo "Introduzca la contraseña de la Base de Datos: "
 
    cat commerce_db.sql | mysql -p

else
    cd /var/www/html

    wget https://ftp.drupal.org/files/projects/commerce_kickstart-7.x-2.52-core.tar.gz

    tar -xzf commerce_kickstart-7.x-2.52-core.tar.gz

    mv commerce_kickstart-7.x-2.52 drupal

    cd /var/www/html/drupal/sites/default/

    cp default.settings.php settings.php
fi

chown -R apache:apache /var/www/html/drupal/

chcon -R -t httpd_sys_content_rw_t /var/www/html/drupal/sites/

firewall-cmd --permanent --add-port=80/tcp
firewall-cmd --permanent --add-port=443/tcp

service firewalld restart

service httpd restart


